#include <opencv2\opencv.hpp>
#include <vector>
#include <time.h>

#pragma once

using namespace std;

class ImageProcessor
{
public:
	ImageProcessor(void);
	~ImageProcessor(void);

	
	//static const int MIN_CONTOUR_SIZE = 4;
	
private:
	CvCapture *capture;
	IplImage *createGrayscaleImage(IplImage *currentFrame);
	IplImage *createBlurredImage(IplImage *currentFrame);
	IplImage *createThresholdImage(IplImage *currentFrame);
	IplImage *createCannyImage(IplImage *currentImage);
	IplImage *createHSVImage( IplImage *currentFrame );
	
	void splitHSVImage( IplImage *currentFrame , IplImage *hueFrame , IplImage *saturationFrame , IplImage *valueFrame );

	int runningTotal;
	int frameCount;
	time_t lastOutput;

	int preset;

	int binaryThreshold;
	int gaussianBlur;
	int cannyThreshold1;
	int cannyThreshold2;
	int minContourSize;
	int lengthToWidthThreshold;
	int circularityThreshold;

	static void presetTrackbarChange( int pos );
	void setupUI();

	bool createCaptureObject();
	void startProcessingLoop();
	bool processingLoop(IplImage *currentFrame);
	void processImage(IplImage *currentFrame);
	
	bool contourIsCircular( CvSeq *contor );

	bool getUserInput();
	
	void drawContoursOnImage( vector<CvSeq*> *contourList , IplImage *outputImage );

	vector<CvSeq*> *createVector( CvSeq *contours );

	void outputHueValue( IplImage *hueFrame , int xPos , int yPos );

	void outputTotal( int total );
};

