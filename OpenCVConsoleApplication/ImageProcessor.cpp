#include "StdAfx.h"
#include "ImageProcessor.h"
#include <opencv2\opencv.hpp>
#include <string>
#include <opencv2\highgui\highgui.hpp>
#include <vector>
#include <time.h>

using namespace std;
using namespace cv;

#define SHOW_ALL_DEBUG_WINDOWS
#define SHOW_CONTROL_WINDOW

/*
Return some presets to the console that were useful whilst
developing the system
*/
void ImageProcessor::presetTrackbarChange(int pos)
{
	string msg;

	switch (pos)
	{
	case 0:
		msg = "Preset0: 3, 100, 100, 200, 4";
		break;
	case 1:
		msg = "Preset1: 5, 110, 178, 175, 9";
		break;
	default:
		msg = "no preset found";
		break;
	}
	
	msg = msg + "\n";

	cout << msg;
}

/*
Set up the user interface (if runtime changes are allowed)
*/
void ImageProcessor::setupUI()
{
	preset = 0;

	gaussianBlur = 11;
	binaryThreshold = 64;
	cannyThreshold1 = 72;
	cannyThreshold2 = 28;
	minContourSize = 10;
	lengthToWidthThreshold = 4;
	circularityThreshold = 8; // ( 1 / 8 = 0.125)

#ifdef SHOW_CONTROL_WINDOW
	cvNamedWindow( "Control", CV_WINDOW_NORMAL );

	cvCreateTrackbar( "Preset" , "Control" , &preset , 3 , presetTrackbarChange );
	cvCreateTrackbar( "Gaussian blur" , "Control" , &gaussianBlur , 15 );
	cvCreateTrackbar( "Binary Threshold" , "Control" , &binaryThreshold , 255 );
	cvCreateTrackbar( "Canny low threshold" , "Control" , &cannyThreshold1 , 255 );
	cvCreateTrackbar( "Canny high threshold" , "Control" , &cannyThreshold2 , 255 );
	cvCreateTrackbar( "Min contour size" , "Control" , &minContourSize , 20 );
	cvCreateTrackbar( "Length to width threshold" , "Control" , &lengthToWidthThreshold , 5 );
	cvCreateTrackbar( "Circularity threshold" , "Control" , &circularityThreshold , 15 );
#endif
}


/*
Default constructor
*/
ImageProcessor::ImageProcessor( void )
{
	//set up open cv ui components
	setupUI();

	//set up the camera
	createCaptureObject();

	//Used for calculating averages
	runningTotal = 0;
	frameCount = 0;
	lastOutput = time(0);

	startProcessingLoop();
}

/*
Destructor - destroy asscoiated objects
*/
ImageProcessor::~ImageProcessor(void)
{
	cvDestroyAllWindows();
	
	cvReleaseCapture( &capture );

	cout << "Destroyed ImageProcessor\n";
}

/*
Entry method for processing - starts an infinite loop
*/
void ImageProcessor::startProcessingLoop()
{
	IplImage *currentFrame;

	//Loop whilst a frame is available
	while ( currentFrame = cvQueryFrame( capture ) )
	{
		//Or if we recieve a command to quit
		if ( processingLoop(currentFrame) ) break;
	}

	cvReleaseImage( &currentFrame );
}

/*
Processing loop - loops as fast as the system can handle
*/
bool ImageProcessor::processingLoop(IplImage *currentFrame)
{
	IplImage *grayscaleImage = NULL;
	IplImage *hsvImage = NULL;
	IplImage *blurredImage = NULL;
	IplImage *thresholdImage = NULL;
	IplImage *cannyImage = NULL;
	IplImage *outputImage = NULL;
	
	IplImage *hueImage = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );
	IplImage *saturationImage = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );
	IplImage *valueImage = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );
	
	CvSeq *contours = NULL;
	vector<CvSeq*> *contourList = NULL;

	CvMemStorage *memStorage = cvCreateMemStorage();
	bool result = false;

	//Processing chain
	// RGB >> HSV >> Sat >> Blurred >> Binary Threshold >> Contour Detection

	//Swap the colour space and split into individual streams
	hsvImage = createHSVImage( currentFrame );
	splitHSVImage( hsvImage , hueImage , saturationImage , valueImage );
	
	//Dump the HSV values to the console
	//outputHueValue( hsvImage , hsvImage->width / 2 , hsvImage->height / 2 ); 

	//Blue the Saturation image to remove some of the noise
	blurredImage = createBlurredImage( saturationImage );

	//Threhold the blurred image to create a binary representation
	thresholdImage = createThresholdImage( blurredImage );

	//Detect the contours and put them into a vector so that we can 
	//further process them
	cvFindContours( thresholdImage , memStorage , &contours );
	
	//Put the contours into a vector (and remove noise)
	contourList = createVector( contours );

	//Draw the contours on a clone of the original image
	outputImage = cvCloneImage( currentFrame );
	drawContoursOnImage( contourList , outputImage );

	//Calculate the number of contour based upon the size of the vector
	outputTotal( contourList->size() );

	//Clean up to avoid memory leaks
	if (memStorage != NULL) cvReleaseMemStorage( &memStorage );
	
	if (hsvImage != NULL) cvReleaseImage( &hsvImage );
	if (grayscaleImage != NULL) cvReleaseImage( &grayscaleImage );
	if (blurredImage != NULL) cvReleaseImage( &blurredImage );
	if (thresholdImage != NULL) cvReleaseImage( &thresholdImage );
	if (cannyImage != NULL) cvReleaseImage( &cannyImage );
	if (outputImage != NULL) cvReleaseImage( &outputImage );
	if ( hueImage != NULL ) cvReleaseImage( &hueImage );
	if ( saturationImage != NULL ) cvReleaseImage( &saturationImage );
	if ( valueImage != NULL ) cvReleaseImage( &valueImage );
	
	//Finished with the list of contours
	delete( contourList );
	
	//Do a check for user input and return a value of true if we should quit
	return ( getUserInput() );
}

/*
Draw the indentified dice pip contours on the output image
*/
void ImageProcessor::drawContoursOnImage( vector<CvSeq*> *contourList , IplImage *outputImage )
{
	//Check there is something to work with
	if ( outputImage != NULL )
	{
		CvSeq *currentContour;

		//Loop through all the contours in the image
		for ( unsigned int i = 0; i < contourList->size(); i++ )
		{
			currentContour = contourList->at( i );

			//Work out the (rough) area of the contour
			CvBox2D minRect = cvMinAreaRect2( currentContour , currentContour->storage );
			CvPoint centrePoint = cvPoint( int( minRect.center.x ) , int( minRect.center.y ));
			CvFont outputFont = cvFont( 2 );
			string label = to_string( ( unsigned long long) i );

			//Draw the current contour
			cvDrawContours( outputImage , currentContour , cvScalarAll(0) , cvScalarAll(255) , -1 , 1 , 8 );
			//Draw the centre point
			cvCircle( outputImage , centrePoint , 2 , cvScalarAll( 255 ));
			//Label the contour
			cvPutText( outputImage , label.c_str() , centrePoint , &outputFont , cvScalarAll( 0 ) );
		}

		cvShowImage( "Output Image" , outputImage );
	}
}

/*
Create a vector using only contours that are accepted as valid
*/
vector<CvSeq*> *ImageProcessor::createVector( CvSeq *contours )
{
	vector<CvSeq*> *result = new vector<CvSeq*>();
	CvSeq *currentContour;

	for ( currentContour = contours; currentContour != NULL ; currentContour = currentContour->h_next )
	{
		//Work out the (rough) area of the contour
		CvBox2D minRect = cvMinAreaRect2( currentContour , contours->storage );

		//Discard contours considered as noise
		if (( minRect.size.height > minContourSize ) && ( minRect.size.width > minContourSize ))
		{
			//For speed just check that the contour (roughly) fits into a square
			if (( minRect.size.height >= minRect.size.width - lengthToWidthThreshold ) && ( minRect.size.height <= minRect.size.width + lengthToWidthThreshold ))
			{
				//Discard contours which are not circular
				if (contourIsCircular(currentContour))
				{
					result->push_back( currentContour );
				}
			}
		}
	}
	return result;
}

/*
Check the contour is circular based on an estimate of what it should be compared to
the actual length of the contour
*/
bool ImageProcessor::contourIsCircular( CvSeq *contour )
{
	bool result;

	//Find the bounding area of the contour
	CvBox2D minRect = cvMinAreaRect2( contour , contour->storage );
	
	//diameter is the average of the height and width of the bounding rectangle
	float diameter = (minRect.size.height + minRect.size.width) / 2;
				
	//make an estimate of the circumference
	float circumference = 3.14159265359 * diameter;
			
	//get the actual contour length
	float actualLength = cvArcLength( contour , CV_WHOLE_SEQ , true );
				
	//create a percentage of error from the circularity threshold taskbar value
	float circularityThresholdPercentage = ( 1 / (float)circularityThreshold);

	//cout << circularityThresholdPercentage << "\n";

	//check the predicted circumfenerence is within range of the actual contour length
	if (( actualLength > (circumference * (1 - circularityThresholdPercentage))) && ( actualLength < (circumference * (1 + circularityThresholdPercentage))))
	{
		result = true;
	}
	else
	{
		result = false;
	}
	//cout << "Contour length: " << cvArcLength( currentContour , CV_WHOLE_SEQ , true ) << "\n";
	//cout << "Expected: " << circumference1 << " - " << circumference2 << "\n";
	return result;
}

/*
Wait for a specified time for input from the user - return true if the user wants to quit
*/
bool ImageProcessor::getUserInput()
{
	bool result = false;

	int key = cvWaitKey( 1 );

	if ( ( key == 'q' ) || ( key == 'Q' ) )
	{
		result = true;
	}

	return result;
}

/*
Create a grayscale image from the current frame
*/
IplImage *ImageProcessor::createGrayscaleImage(IplImage *currentFrame)
{
	IplImage *result = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );
	
	cvCvtColor( currentFrame, result, CV_BGR2GRAY );

#ifdef SHOW_ALL_DEBUG_WINDOWS 
	if ( result != NULL ) cvShowImage( "Grayscale image" , result );
#endif

	return result;
}

/*
Blur the image to remove some noise, using a gaussian blur
*/
IplImage *ImageProcessor::createBlurredImage(IplImage *currentFrame)
{
	IplImage *result = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );
	int blurAmount;
	
	//Gaussian blur must be an odd value so remove any even values
	if ( gaussianBlur % 2 == 0)
	{
		blurAmount = gaussianBlur - 1;
	}
	else
	{
		blurAmount = gaussianBlur;
	}
	
	//OpenCV will throw an exception if gaussian blur parameter is set to zero
	if ( gaussianBlur > 0)
	{
		cvSmooth( currentFrame , result , CV_GAUSSIAN , blurAmount , blurAmount );
	}

#ifdef SHOW_ALL_DEBUG_WINDOWS 
	if ( result != NULL ) cvShowImage( "Blurred Image" , result );
#endif

	return result;
}

/*
Threshold the image to create a binary representation of the saturation
*/
IplImage *ImageProcessor::createThresholdImage(IplImage *currentFrame)
{
	IplImage *result = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );

	cvThreshold( currentFrame , result , this->binaryThreshold , 255 , CV_THRESH_BINARY );

#ifdef SHOW_ALL_DEBUG_WINDOWS 
	if ( result != NULL ) cvShowImage( "Thresholded Image" , result );
#endif

	return result;
}

/*
Do canny edge detection on the image
*/
/*
IplImage *ImageProcessor::createCannyImage(IplImage *currentFrame)
{
	IplImage *result = cvCreateImage( cvGetSize( currentFrame ) , 8 , 1 );

	cvCanny( currentFrame , result , cannyThreshold1 , cannyThreshold2 );

#ifdef SHOW_ALL_DEBUG_WINDOWS 
	if ( result != NULL ) cvShowImage( "Canny Image" , result );
#endif

	return result;
}
*/

/*
Create a link to the webcam
*/
bool ImageProcessor::createCaptureObject()
{
	bool result = true;

	capture = cvCreateCameraCapture( 0 );

	cout << "Initialised camera capture\n";

	return result;
}

/*
Switch from RGB to HSV colour space
*/
IplImage *ImageProcessor::createHSVImage( IplImage *currentFrame )
{
	IplImage *result = cvCreateImage( cvGetSize( currentFrame ) , 8 , 3 );
	
	cvCvtColor( currentFrame , result , CV_RGB2HSV );

#ifdef SHOW_ALL_DEBUG_WINDOWS 
	if ( result != NULL ) cvShowImage( "HSV Image" , result );
#endif

	return result;
}

/*
Return only the saturation frame and discard the hue and value frames
*/
void ImageProcessor::splitHSVImage( IplImage *currentFrame , IplImage *hueFrame , IplImage *saturationFrame , IplImage *valueFrame )
{
	if ( currentFrame != NULL )
	{
		cvSplit( currentFrame , hueFrame , saturationFrame , valueFrame , NULL );

#ifdef SHOW_ALL_DEBUG_WINDOWS
		if ( hueFrame != NULL ) cvShowImage( "H channel" , hueFrame );
		if ( saturationFrame != NULL ) cvShowImage( "S channel" , saturationFrame );
		if ( valueFrame != NULL ) cvShowImage( "V channel" , valueFrame );
#endif
	}	
}

/*
Calculate the average number of spots over 100 frames
*/
void ImageProcessor::outputTotal( int total )
{
	if ( frameCount != 100 )
	{
		runningTotal += total;
		frameCount++;
		
		//cout << frameCount << "\n";
	}
	else
	{
		float calculatedAverage = runningTotal / frameCount;

		cout << "Spots: " << calculatedAverage << "\n";

		runningTotal = 0;
		frameCount = 0;
	}
}

/*
Output the hue value of the pixel at (xpos, ypos)
*/
void ImageProcessor::outputHueValue( IplImage *hueFrame , int xPos , int yPos )
{
	CvScalar s;

	s = cvGet2D( hueFrame , xPos , yPos );

	//cout << s.val[0] << "," << s.val[1] << "," << s.val[2] << "\n";
}