// OpenCVConsoleApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include "ImageProcessor.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	//Create an instance of the image processor and pass control to it
	ImageProcessor *imageProcessor = new ImageProcessor();

	//Destroy the image processor when its finished with
	delete(imageProcessor);

	return 0;
}

